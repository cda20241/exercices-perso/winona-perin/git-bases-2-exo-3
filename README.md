**status**

conflit non résolu

"CONFLIT (contenu) : Conflit de fusion dans main.py"

"La fusion automatique a échoué ; réglez les conflits et validez le résultat."


commandes utilisées dans cet exercice:

**NAVIGATION**
- pwd
- ls
- cd [file-name]

**GIT:**
- git clone [url]
- git status
- git branch [branch-name]
- git checkout [branch-name]
- git add [file-name]
- git add .
- git commit -m "" [file-name]
- git push [origin] [branch-name]
- git merge [branch-name]
- git pull origin [branch-name]

**VIM:**
- vim [file-name]
- :x
- :q
